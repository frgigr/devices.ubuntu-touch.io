# Ubuntu Touch devices

The new new [Ubuntu Touch devices website](https://devices.ubuntu-touch.io)! Made using [Gridsome](https://gridsome.org/). You can start a development server by running `npm run develop`.

## Where did my device configuration go?

Device files have been moved to the [data/devices](data/devices) folder.

## Where did my Markdown content go? It's not displaying on the live webpage!

We have currently disabled Markdown rendering on each device's page given the changes in how device information is presented. Once we have most of the devices moved from the old (everything in the markdown) method to the new (most information in the YAML) method, we will re-enable rendering.

The [Adding new devices section](#adding-new-devices) will help you update your device's configuration file to the new format.

## Adding new devices

You can add new devices by adding a markdown file with a YAML metadata block under `/devices/<codename>.md`. The YAML metadata block is a snippet of YAML in your otherwise Markdown-formatted document, and is denoted by three dashes alone on the line above and below the block.

```md
---
name: My Ported Device
deviceType: phone
...
---

## You should know...

This device occasionally requests an offering of bacon. Don't ask, just provide.

## Preinstallation instructions

Before using the UBports Installer on this device, ...
```

Your YAML metadata block will have three primary sections. For more information on filling these sections, see the [YAML metadata block reference](#yaml-metadata-block-reference) below.

### Device specifications

Device specifications should be placed in the deviceInfo section of the YAML header.

### Compatibility information

Information about working or broken features should be placed in the portStatus section. See [Feature IDs](#feature-ids) for a full list of available feature IDs.

### Preinstallation instructions

The general markdown section of the file is now available for preinstallation instructions for your device. For example, if your device needs to have a certain version of Android installed prior to running the UBports Installer, those instructions should be written in Markdown after the metadata block.

Rendering of this Markdown is currently disabled. We will re-enable it after most of the devices listed on the site have migrated to the new format.

## YAML metadata block reference

The YAML metadata block should have the following format; variables in square brackets are `[optional]`:

```yml
name: <retail name of the device>
deviceType: <phone|tablet|tv|other>
maturity: <deprecated: number between 0 and 1 indicating quality of the port>
[installerAlias: <codename of the device in the installer (leave out if it is the same as file name)>]
[installLink: <link to install instructions (repo), if installer isn't available>]
[buyLink: <link to manufacturer's store, if it is available>]
[tag: <promoted|old>]
[
portStatus:
  - categoryName: <name of the category of the features>
    features:
      - id: <feature ID, you can find the complete list below>
        value: < + | - >
        bugTracker: <link to the relevant issue report if this feature is not working>
]
[
deviceInfo:
  - id: <device specification ID, you can find the complete list below>
    value: <device technical specification, model...>
]
[
contributors:
  - name: <contributor name>
    photo: <link to avatar picture>
    forum: <link to ubports forum profile>
]
[
externalLinks:
  - name: <link label>
    link: <target>
    icon: <icon for the link> # Available icons can be found in the static/img/ folder
]
```

### Feature IDs

```yaml
portStatus:
  - categoryName: "Sound"
    - id: "earphones"
      value: "-"
      bugTracker: "https://link to my bug report"
```

The portStatus matrix allows the site to display the hardware compatibility of your port. This helps users discover ports that meet their needs and sets expectations for people looking to buy a device for Ubuntu Touch.

If your device has hardware support for a given feature and it can be used under Ubuntu Touch, set the `value:` key to `"+"`. This marks the feature as working.

If your device does not have hardware support for a given feature, set the `value:` key to `"x"`. The site will ignore any missing features when calculating the progress value for your device.

If your device has hardware support for a feature but it does not work when running Ubuntu Touch, set the `value:` key to `"-"`. This marks the feature as broken.

Mark a feature as partially working if your device has hardware support for the feature *and* it works most of the time when running Ubuntu Touch, *but* most users will encounter a critical issue with the feature within 1 week of booting the device. The `value:` key for partially working features is `"+-"`

If you haven't tested a feature or you don't know its state you can set the `value:` key to `"?"`.

If possible, include a bug report link with the `bugTracker` key when marking a feature as broken or partially working.

#### Actors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| manualBrightness     | Manual brightness                       |
| notificationLed      | Notification LED                        |
| torchlight           | Torchlight                              |
| vibration            | Vibration                               |

#### Camera

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| flashlight           | Flashlight                              |
| photo                | Photo                                   |
| video                | Video                                   |
| switchCamera         | Switching between cameras               |

#### Cellular

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| carrierInfo          | Carrier info, signal strength           |
| dataConnection       | Data connection                         |
| calls                | Incoming, outgoing calls                |
| mms                  | MMS in, out                             |
| pinUnlock            | PIN unlock                              |
| sms                  | SMS in, out                             |
| audioRoutings        | Change audio routings (Enable speakerphone) |
| voiceCall            | Voice in calls                          |

#### Endurance

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| batteryLifetimeTest  | Battery lifetime > 24h from 100% (Suspended, Wi-Fi, mobile data on if available) |
| noRebootTest         | No reboot needed for 1 week             |

#### GPU

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| uiBoot               | Boot into UI                            |
| videoAcceleration    | Hardware video playback                 |

#### Misc

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| anboxPatches         | Anbox patches                           |
| apparmorPatches      | AppArmor patches                        |
| batteryPercentage    | Battery percentage                      |
| offlineCharging      | Offline charging                        |
| onlineCharging       | Online charging                         |
| recoveryImage        | Recovery image                          |
| factoryReset         | Reset to factory defaults               |
| rtcTime              | RTC time                                |
| sdCard               | SD card detection and access            |
| shutdown             | Shutdown / Reboot                       |
| wirelessCharging     | Wireless charging                       |
| wirelessExternalMonitor | Wireless external display support (Miracast) |

#### Network

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| bluetooth            | Bluetooth                               |
| flightMode           | Flight mode                             |
| hotspot              | Hotspot                                 |
| nfc                  | NFC                                     |
| wifi                 | WiFi                                    |

#### Sensors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| autoBrightness       | Automatic brightness                    |
| fingerprint          | Fingerprint reader                      |
| gps                  | GPS                                     |
| proximity            | Proximity                               |
| rotation             | Rotation                                |
| touchscreen          | Touchscreen                             |

#### Sound

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| earphones            | Earphones                               |
| loudspeaker          | Loudspeaker                             |
| microphone           | Microphone                              |
| volumeControl        | Volume control                          |

#### USB

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| mtp                  | MTP access                              |
| adb                  | ADB access                              |
| wiredExternalMonitor | Wired external display support (HDMI, DisplayPort, SlimPort, MHL) |

### Device specification IDs

```
deviceInfo:
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
```

| Specification ID     | Specification name                      |
| :------------------- | :-------------------------------------: |
| cpu                  | CPU                                     |
| chipset              | Chipset                                 |
| gpu                  | GPU                                     |
| rom                  | Storage                                 |
| ram                  | Memory                                  |
| android              | Android Version                         |
| battery              | Battery                                 |
| display              | Display                                 |
| rearCamera           | Rear Camera                             |
| frontCamera          | Front Camera                            |
| arch                 | Architecture                            |
| dimensions           | Dimensions                              |
| weight               | Weight                                  |

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Example device.md

```yaml
---
name: "Your Ported Device"
deviceType: "phone"
installerAlias: "portedDevice"
installLink: "https://github.com"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "+"
      - id: "wirelessExternalMonitor"
        value: "+"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/245"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "+"
deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
  - id: "gpu"
    value: "Qualcomm Adreno 510"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 6.0.1"
  - id: "battery"
    value: "2620 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "23MP"
  - id: "frontCamera"
    value: "13MP"
contributors:
  - name: Me
    photo: https://gravatar.com/me
    forum: https://forums.ubports.com/user/testmoderator01
externalLinks:
  - name: "Telegram"
    link: "https://t.me/joinchat/"
    icon: "telegram"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/"
    icon: "yumi"
  - name: "Report a bug"
    link: "https://github.com/ubports/ubuntu-touch/issues"
    icon: "github"
  - name: "Device source"
    link: "https://github.com/"
    icon: "github"
---
```
