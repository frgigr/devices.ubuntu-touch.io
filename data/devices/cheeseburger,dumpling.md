---
name: "Oneplus 5/5T"
deviceType: "phone"
image: "https://wiki.lineageos.org/images/devices/cheeseburger.png"
maturity: .6

contributors:
  - name: 'Vince1171'
    photo: ''
    forum: '#'
  - name: 'Jami'
    photo: ''
    forum: '#'
  - name: 'Flohack'
    photo: ''
    forum: '#'

externalLinks:
  - name: 'Forum Post'
    link: 'https://forums.ubports.com/category/53/oneplus-5-5t'
    icon: 'yumi'
  - name: 'Repository: Oneplus 5'
    link: 'https://github.com/Flohack74/android_device_oneplus_cheeseburger'
    icon: 'github'
  - name: 'Repository: Oneplus 5T'
    link: 'https://github.com/Flohack74/android_device_oneplus_dumpling'
    icon: 'github'
---
