/**
 * Ubuntu Touch devices website
 * Copyright (C) 2021 UBports Foundation <info@ubports.com>
 * Copyright (C) 2021 Jan Sprinz <neo@neothethird.de>
 * Copyright (C) 2021 Riccardo Riccio <rickyriccio@zoho.eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module.exports = {
  siteName: "Ubuntu Touch • Linux Phone",
  titleTemplate: "%s • Ubuntu Touch • Linux Phone",
  siteUrl: "https://devices.ubuntu-touch.io",
  plugins: [
    { use: "gridsome-plugin-svg" },
    {
      use: "@gridsome/source-filesystem",
      options: {
        path: "./data/devices/*.md",
        typeName: "Device",
        pathPrefix: "/device",
        remark: {}
      }
    },
    {
      use: "gridsome-plugin-flexsearch",
      options: {
        searchFields: ["name"],
        autoFetch: false,
        collections: [
          {
            typeName: "Device",
            indexName: "Device",
            fields: [
              "deviceType",
              "codename",
              "path",
              "name",
              "progress",
              "tag"
            ]
          }
        ]
      }
    },
    {
      use: "gridsome-plugin-matomo",
      options: {
        host: "https://analytics.ubports.com",
        siteId: 3
      }
    },
    {
      use: "@gridsome/plugin-sitemap",
      options: {
        config: {
          "/device/*": {
            changefreq: "weekly",
            priority: 0.7
          }
        }
      }
    },
    {
      use: "gridsome-plugin-robots",
      options: {
        policy: [{ userAgent: "*", allow: "/" }]
      }
    }
  ],
  transformers: { remark: {} },
  templates: {
    Device: "/device/:fileInfo__name"
  }
};
